# hutan: An R package for manipulation of phylogenetic trees

This repository has moved to [github](https://github.com/caseywdunn/hutan). Please follow the link for the current version.