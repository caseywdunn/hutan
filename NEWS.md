# hutan 0.1.0

- First pass at hutan, motivated largely by the desire to implment `zero_constrained()` and `is_monophyletic()`. Other functions were created largely in service of these two.
