## Test environments
* OS X 10.10, R 3.2.0
* ubuntu 15.04, R 3.1.2

## R CMD check results
There were no ERRORs or WARNINGs. 

There was 1 NOTE:

* checking dependencies in R code ... NOTE
Namespace in Imports field not imported from: ‘ape’
  All declared Imports should be used.


## Downstream dependencies
I have not run R CMD check on downstream dependencies of hutan.
